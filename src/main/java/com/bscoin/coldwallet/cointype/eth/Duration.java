package com.bscoin.coldwallet.cointype.eth;

/**
 * Created by wulei on 2015-08-18.
 */
public class Duration {

    public static final long SECOND_MILLIS = 1000;
    public static final long MINUTE_MILLIS = 60*SECOND_MILLIS;
    public static final long HOUR_MILLIS=60*MINUTE_MILLIS;
    public static final long DAY_MILLIS = 24*HOUR_MILLIS;

    private long time =0;
    private Duration(long time){
        this.time = time;
    }
    public static Duration ofMillis(long time){
        return new Duration(time);
    }
    public int toDays(){
        return (int)(time/DAY_MILLIS);
    }
    public Duration minusDays(int d){
        time = time-d*DAY_MILLIS;
        return this;
    }
    public int toHours(){
        return (int)(time/HOUR_MILLIS);
    }
    public int toMinutes(){
        return (int)(time/MINUTE_MILLIS);
    }
    public int getSeconds(){
        return (int)(time/SECOND_MILLIS);
    }

    public Duration minusHours(int h){
        return new Duration(time-h*HOUR_MILLIS);
    }
    public Duration minusMinutes(int m){
        return new Duration(time-m*MINUTE_MILLIS);
    }

    public long getMillis(){
        return time;
    }

    public static void main(String[] args) {
       /* Duration d =  Duration.ofMillis((long) (Duration.HOUR_MILLIS * 2 + 50 * Duration.MINUTE_MILLIS));
        int h = d.toHours();
        System.out.println(d.minusHours(h).getMillis());
        int m =  d.minusHours(h).toMinutes();
        System.out.println(d.minusHours(h).getMillis());
        System.out.println(d.minusHours(h).getMillis()); System.out.println(d.minusHours(h).getMillis()); System.out.println(d.minusHours(h).getMillis());


        System.out.println(h+"小时"+m+"分");*/
    	System.out.println(HOUR_MILLIS);
    }

}
