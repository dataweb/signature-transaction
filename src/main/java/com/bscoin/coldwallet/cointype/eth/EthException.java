package com.bscoin.coldwallet.cointype.eth;

public class EthException extends RuntimeException {
	
	private static final long serialVersionUID = -2400177231186534876L;
	
	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public EthException(String msg) {
		super();
		this.msg = msg;
	}

	@Override
	public String getMessage() {
		return this.getMsg();
	}

}
