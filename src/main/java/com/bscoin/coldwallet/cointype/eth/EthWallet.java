package com.bscoin.coldwallet.cointype.eth;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;


public class EthWallet {
	private static Logger logger = LoggerFactory.getLogger(EthWallet.class);
	
	//private static ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
	
	public final Long GAS_LIMIT = 25000L; 
	
	
	public static Map<String,String> createWallet() throws Exception{
			ECKeyPair ecKeyPair = Keys.createEcKeyPair();
			//WalletFile walletFile = Wallet.createLight(password, ecKeyPair);
			//System.out.println("address " + walletFile.getAddress());
			String privateKey = ecKeyPair.getPrivateKey().toString(16);
			
			String publicKey = ecKeyPair.getPublicKey().toString(16);
			String address = Keys.getAddress(ecKeyPair.getPublicKey());
			Map<String,String> result = new HashMap<String,String>();
			result.put("privateKey", privateKey);
			result.put("publicKey", publicKey);
			result.put("address", "0x"+address);
			
			return result;
	}
	
	public static String signTransaction(String from,BigInteger nonce, BigInteger gasPrice, BigInteger gasLimit, String to,
			BigInteger value,  String privateKey) throws IOException {
		logger.info("=======================signTransaction==========================");
		logger.info("nonce:"+nonce);
		logger.info("gasPrice:"+gasPrice);
		logger.info("gasLimit:"+gasLimit);
		logger.info("to:"+to);
		logger.info("value:"+value);
		logger.info("privateKey:"+privateKey);
		logger.info("from:"+from);
		if (privateKey.startsWith("0x")) {
			privateKey = privateKey.substring(2);
		}
		ECKeyPair ecKeyPair = ECKeyPair.create(new BigInteger(privateKey, 16));
		
		Credentials credentials = Credentials.create(ecKeyPair);
		String signData =  signTransaction(nonce, gasPrice, gasLimit, to, value, "", credentials);
		logger.info("=======================signTransaction(end)=======================");
		return signData;
	}

	public static String signTransaction(BigInteger nonce, BigInteger gasPrice, BigInteger gasLimit, String to,
			BigInteger value, String data, Credentials credentials) throws IOException {
		byte[] signedMessage;
		RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit, to, value, data);
	
		signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
		String hexValue = Numeric.toHexString(signedMessage);
		logger.info("signedData : " + hexValue);
		return hexValue;
	}
	
	
	public static BigDecimal fromEthtoWei(BigDecimal eth) {
		return Convert.toWei(eth, Convert.Unit.ETHER);
	}
	
	public static BigDecimal fromGWeitoWei(BigDecimal gwei) {
		return Convert.toWei(gwei, Convert.Unit.GWEI);
	}
	
	
	public static void main(String[] args) throws Exception {
		Map<String,String> result = createWallet();
		System.out.println(JsonUtil.toJson(result));
		
	}

}
 