package com.bscoin.coldwallet.cointype.eth;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;


public class JsonUtil {

	public static String toJson(Object object) {
		return toJson(object, DateUtil.DF_FULL_S1.toPattern());

	}
	
	public static JSONObject parseJsonObject(String json,String... keys) {
		JSONObject jsonObject =  JSON.parseObject(json);
		
		for(String key : keys) {
			jsonObject = jsonObject.getJSONObject(key);
		}
		return jsonObject;
	}
	
	public static JSONObject parseJsonObject(JSONObject object ,String...keys ) {
		JSONObject jsonObject =  object;
		
		for(String key : keys) {
			jsonObject = jsonObject.getJSONObject(key);
		}
		return jsonObject;
	}

	/**
	 * 
	 * @param json
	 *            json串
	 * @param clazz
	 *            生成的bean class
	 * @return 生成对象
	 */
	public static <T> T toObject(String json, Class<T> clazz) {
		return JSON.parseObject(json, clazz);
	}

	/**
	 * 
	 * @param json
	 *            json串
	 * @param clazz
	 *            生成的List的元素class
	 * @return List<T>
	 */
	public static <T> List<T> toArray(String json, Class<T> clazz) {

		return JSON.parseArray(json, clazz);
	}

	/**
	 * 
	 * @param object
	 *            对象
	 * @param dateFormat
	 *            日期字符串
	 * @return
	 */
	public static String toJson(Object object, String dateFormat) {
		return JSON.toJSONStringWithDateFormat(object, dateFormat, SerializerFeature.WriteDateUseDateFormat);

	}

}
