/**   
 * @Title: DateUtil.java 
 * @Package com.myapp.security.util 
 * @Description: TODO 添加描述
 * @author wulei   
 * @date 2011-8-31 下午08:39:37  
 */
package com.bscoin.coldwallet.cointype.eth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final SimpleDateFormat DF_FULL_S1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * yyyy年MM月dd日 HH时mm分ss秒
	 */
	public static final SimpleDateFormat DF_FULL_S2 = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
	/**
	 * yyyy-MM-dd
	 */
	public static final SimpleDateFormat DF_ONLY_YMD_S1 = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * yyyy年MM月dd日
	 */
	public static final SimpleDateFormat DF_ONLY_YMD_S2 = new SimpleDateFormat("yyyy年MM月dd日");

	public static Date toDate(String str, SimpleDateFormat simpleDateFormat) {
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String toStr(Date date, SimpleDateFormat simpleDateFormat) {
		return simpleDateFormat.format(date);
	}

	public static String toFullS1Str(Date date) {
		return DF_FULL_S1.format(date);
	}

	public static String toShortS1Str(Date date) {
		return DF_ONLY_YMD_S1.format(date);
	}

	public static Date getWeekOfFirst(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);

		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		return cal.getTime();
	}

	public static Date getWeekOfEnd(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int d = 0;
		if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
			d = -6;
		} else {
			d = 2 - cal.get(Calendar.DAY_OF_WEEK);
		}
		cal.add(Calendar.DAY_OF_WEEK, d);
		cal.add(Calendar.DAY_OF_WEEK, 6);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 58);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	public static Date getNextDate(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + 1);
		return c.getTime();
	}

	public static Date getDayLastTime(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 58);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}
	
	/* 
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
 
}
