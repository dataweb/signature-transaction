package com.bscoin.coldwallet.cointype.common;  
  
import java.io.UnsupportedEncodingException;  
import java.security.MessageDigest;  
import java.security.NoSuchAlgorithmException;  
  
/** 
 * 采用MD5加密解密 
 * @author tfq 
 * @datetime 2011-10-13 
 */  
public class SecretOperation {  
    /** 
     * 加密解密算法 执行一次加密，两次解密 
     */   
    public static String convertMD5(String inStr){  
  
        char[] a = inStr.toCharArray();  
        for (int i = 0; i < a.length; i++){  
            a[i] = (char) (a[i] ^ 't');  
        }  
        String s = new String(a);  
        return s;  
  
    }  
  
    // 测试主函数  
    public static void main(String args[]) {  
        String s = new String("Kxd4jYoPpwcJtVr8UUtrGosDEEzWg9buN5kL9ZLvh7fRojvf2oYT");  
        System.out.println("原始：" + s);  
        System.out.println("加密的：" + convertMD5(s));  
        System.out.println("解密的：" + convertMD5(convertMD5(s)));  
        System.out.println(convertMD5("\\u0017'\\u0016>\\u001CM\\rF6BA\\u001E\\f\\u0010\\!\\u0006\\r&9\\u001F\\u0017\\u0006\\u0019L?C\\u0002\\u0013\\:72A\u0010'F\u0015M-\u0001-\u00151E\u000E \u001B\u0001\r\u001E\u001D,\f"));
        System.out.println(convertMD5("9[@@<[D<"));
    }  
}